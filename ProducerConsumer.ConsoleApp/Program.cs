﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProducerConsumer.ConsoleApp
{
    class Program
    {
        private const bool IsCpuIntensive = false;
        private const bool IsDataIntensive = true;  
        private const double WorkDurationInMs = 10;

        private static string path = @"../../" + (IsDataIntensive ? "test-big.csv" : "test-light.csv");

        static void Main(string[] args)
        {
            var stopwatch = new Stopwatch();
            long total;

            //Start(stopwatch);
            //total = DoSync();
            //Stop(stopwatch, total, "Synchronous work ");

            Start(stopwatch);
            total = DoConsumerPerLine();
            Stop(stopwatch, total, "Consumer per line");

            Start(stopwatch);
            total = DoProducerConsumer();
            Stop(stopwatch, total, "Producer Consumer");

            Start(stopwatch);
            total = DoConsumerPerProcessor();
            Stop(stopwatch, total, "Consumer per core");

            Start(stopwatch);
            total = DoParallel();
            Stop(stopwatch, total, "Parallel foreach ");

            Start(stopwatch);
            total = DoPLinq();
            Stop(stopwatch, total, "PLinq query      ");

            Console.ReadLine();
        }

        private static long Work(string line)
        {
            var value = line.Count();
            var result = Compute(value);

            #region cpu intensive

            if (IsCpuIntensive)
            {
                var start = DateTime.Now;
                int counter = 0;
                do
                {
                    if (counter == int.MaxValue) counter = 0;
                    counter += 1;
                } while ((DateTime.Now - start).TotalMilliseconds < WorkDurationInMs);
            }

            #endregion

            return result;
        }

        private static long Compute(int value)
        {
            return value / 4;
        }

        private static long DoSync()
        {
            using (var reader = new FileReader(path))
            {
                long total = 0;
                foreach (var line in reader.Lines)
                {
                    total += Work(line);
                }
                return total;
            }
        }

        private static long DoConsumerPerLine()
        {
            var lines = new BlockingCollection<string>();

            using (var reader = new FileReader(path))
            {
                var consumers = new List<Task<long>>();
                foreach (var line in reader.Lines)
                {
                    consumers.Add(Task.Run(() => Work(line)));
                }

                Task.WaitAll(consumers.ToArray());
                return consumers.Sum(t => t.Result);
            }
        }

        private static long DoProducerConsumer()
        {
            var lines = new BlockingCollection<string>();

            var producerTask = CreateProducer(lines);
            var consumerTask = CreateConsumer(lines);

            consumerTask.Wait();
            return consumerTask.Result;
        }

        private static long DoConsumerPerProcessor()
        {
            var lines = new BlockingCollection<string>();

            var producerTask = CreateProducer(lines);

            var consumersCount = Environment.ProcessorCount;
            if (!IsCpuIntensive) consumersCount -= 1;

            var consumers = Enumerable.Range(0, consumersCount)
                .Select(i => CreateConsumer(lines));

            var complete = Task.WhenAll(consumers)
                .ContinueWith(results => results.Result.Sum());
            complete.Wait();

            return complete.Result;
        }

        private static long DoParallel()
        {
            long total = 0;
            using (var reader = new FileReader(path))
            {
                Parallel.ForEach(reader.Lines,
                    () => 0L,
                    (line, _, tls) => tls + Work(line),
                    tls => total += tls
                );
            }
            return total;
        }

        private static long DoPLinq()
        {
            using (var reader = new FileReader(path))
            {
                return reader.Lines.AsParallel().Select(Work).Sum();
            }
        }

        private static Task<long> CreateConsumer(BlockingCollection<string> lines)
        {
            return Task.Factory.StartNew(() =>
            {
                long total = 0;
                while (lines.IsCompleted == false)
                {
                    try
                    {
                        total += Work(lines.Take());
                    }
                    catch (InvalidOperationException)
                    {
                        //end of lines
                    }
                }
                return total;
            });
        }

        private static Task CreateProducer(BlockingCollection<string> lines)
        {
            return Task.Factory.StartNew(() =>
            {
                using (var reader = new FileReader(path))
                {
                    foreach (var line in reader.Lines)
                    {
                        lines.Add(line);
                    }
                    lines.CompleteAdding();
                }
            });
        }

        private static void Stop(Stopwatch stopwatch, long total, string name)
        {
            Console.WriteLine(name + "(" + total + ") run in: " + stopwatch.ElapsedMilliseconds + " ms");
            stopwatch.Restart();
        }

        private static void Start(Stopwatch stopwatch)
        {
            stopwatch.Start();
        }
    }
}
