﻿using Common;
using Common.DataSource;
using Common.Services;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Tpl.Tasks.CreationOptions
{
    class Program
    {
        static void Main(string[] args)
        {
            PersonneData personneData = new PersonneData();
            IList<IPersonne> personnes = personneData.GetAll();

            #region Display
            Console.WriteLine("[{0}] nb processors : {1}", Thread.CurrentThread.ManagedThreadId, System.Environment.ProcessorCount);
            Thread.Sleep(1000);
            Console.WriteLine("[{0}] Starting...", Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(300);
            #endregion

            DateTime startTime = DateTime.Now;

            // >>> 1. Parallel Foreach
            //ParallelLoopResult result = System.Threading.Tasks.Parallel.ForEach<IPersonne>(personnes, (p) =>
            //{
            //    p.Age = CalculatorService.Compute(2);
            //    Console.WriteLine("[{0}] > Processed n°{1} {2} {3} ...", Thread.CurrentThread.ManagedThreadId, p.Id, p.Prenom, p.Nom);
            //});

            //DateTime endTime = DateTime.Now;
            //TimeSpan elapsedTime = endTime.Subtract(startTime);
            //Console.WriteLine("[{0}] ParallelLoop Foreach completed in {2} ({1})", System.Threading.Thread.CurrentThread.ManagedThreadId, result.IsCompleted, elapsedTime);

            // >>> 2. Task foreach // TaskCreation
            List<Task> tasks = new List<Task>();

            foreach (IPersonne p in personnes)
            {
                Task task = Task.Factory.StartNew(() =>
                        {
                            p.Age = CalculatorService.Compute(2);
                            Console.WriteLine("[{0}] > Processed n°{1} {2} {3} ...", Thread.CurrentThread.ManagedThreadId, p.Id, p.Prenom, p.Nom);
                        } , TaskCreationOptions.LongRunning);

                tasks.Add(task);
            }

            Task.WaitAll(tasks.ToArray());

            DateTime endTime = DateTime.Now;
            TimeSpan elapsedTime = endTime.Subtract(startTime);
            Console.WriteLine("[{0}] Simple Foreach completed in {1}", System.Threading.Thread.CurrentThread.ManagedThreadId, elapsedTime);

            Console.ReadKey();
        }
    }
}
