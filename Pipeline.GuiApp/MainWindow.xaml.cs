﻿using Common;
using Common.DataSource;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Pipeline.GuiApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Starts the progress bar.
        /// </summary>
        private void StartProgressBar()
        {
            Result.Text = "";
            Progress.IsIndeterminate = true;
        }

        /// <summary>
        /// Stops the progress bar.
        /// </summary>
        private void StopProgressBar()
        {
            Progress.IsIndeterminate = false;
        }

        /// <summary>
        /// Handles the OnClick event of the Synchronous control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Sync_OnClick(object sender, RoutedEventArgs e)
        {
            StartProgressBar();

            // Do Three Step Action
            var result1 = DoWorkFirstStep();
            var result2 = DoWorkSecondStep(result1, 30);
            var result3 = DoWorkThirdPart(result2);

            // Harvest Result
            Result.Text = result3.ToString();

            StopProgressBar();
        }


        /// <summary>
        /// Handles the OnClick event of the Task control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Task_OnClick(object sender, RoutedEventArgs e)
        {
            StartProgressBar();

            // Build Pipeline
            Task<IEnumerable<IPersonne>> stepOne = new Task<IEnumerable<IPersonne>>(DoWorkFirstStep);
            Task<IEnumerable<IPersonne>> stepTwo = stepOne.ContinueWith<IEnumerable<IPersonne>>(t => { return DoWorkSecondStep(t.Result, 30); });
            Task<int> stepThree = stepTwo.ContinueWith<int>(t => { return DoWorkThirdPart(t.Result); });

            // Start Tasks
            stepOne.Start();             

            // Harvest result from UI thread 
            stepThree.ContinueWith(t =>
                { 
                    // Harvest Result
                    Result.Text = stepThree.Result.ToString(); 
                    StopProgressBar();
                }, 
                TaskScheduler.FromCurrentSynchronizationContext());
        }

        /// <summary>
        /// Does the first step.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<IPersonne> DoWorkFirstStep()
        {
            PersonneData dataSource = new PersonneData();
            // Load from File
            IEnumerable<IPersonne> personnes = dataSource.GetAllOneByOne();

            return personnes;
        }

        /// <summary>
        /// Does the work second step.
        /// </summary>
        /// <param name="personnes">The personnes.</param>
        /// <param name="ageFilter">The age filter.</param>
        /// <returns></returns>
        private IEnumerable<IPersonne> DoWorkSecondStep(IEnumerable<IPersonne> personnes, int ageFilter)
        {
            IEnumerable<IPersonne> filteredPersonnes = personnes.Where(x => x.Age <= ageFilter).ToList();

            // Simulate delay
            Thread.Sleep(1000);

            return filteredPersonnes;
        }

        /// <summary>
        /// Does the work third part.
        /// </summary>
        /// <param name="personnes">The personnes.</param>
        /// <returns></returns>
        private int DoWorkThirdPart(IEnumerable<IPersonne> personnes)
        {
            Common.Services.CalculatorService.Compute(2);

            return  personnes.Sum<IPersonne>(x => x.Age)/personnes.Count();
        }

        /// <summary>
        /// Handles the OnClick event of the LoadBooks control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void LoadBooks_OnClick(object sender, RoutedEventArgs e)
        {
            CSV_reader reader = new CSV_reader();

            DateTime startTime = DateTime.Now;

            Debug.WriteLine(DateTime.Now);
            
            IList<Book> books = reader.GetBooks();

            DateTime endTime = DateTime.Now;
            TimeSpan elapsedTime = endTime.Subtract(startTime);

            Debug.WriteLine(elapsedTime);

            Result.Text = string.Format("{0} books found", books.Count);
            Time.Text = string.Format("in {0:ss} sec", elapsedTime);
        }
    }
}
