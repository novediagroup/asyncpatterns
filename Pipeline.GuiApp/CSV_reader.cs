﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using System.Timers;
using System.Diagnostics;

namespace Pipeline.GuiApp
{
    class CSV_reader
    {
        public IList<Book> GetBooks()
        {
            IList<Book> books = new List<Book>();

            string currentPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            FileInfo file = new FileInfo(Path.Combine(currentPath,"bibliotheques_localisations_exemplaires_jan_2009.csv"));
            CsvConfiguration csvConfig = new CsvConfiguration(){ Delimiter=";", HasHeaderRecord=true, TrimFields=true};

            CsvHelper.CsvReader reader = new CsvReader(new StreamReader(file.FullName),csvConfig);

            while (reader.Read())
            {
                Book book = Book.Map(reader.CurrentRecord);
                books.Add(book);
            }

            return books;
        }
    }
}
