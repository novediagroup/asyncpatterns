﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pipeline.GuiApp
{
    class Book
    {
        public string Langue;
        public string AnneeEdition;
        public string ISBN;
        public string Indice;
        public string Auteur_personne_physique;
        public string Auteur_collectivite;
        public string Coauteur;
        public string Responsabilite;
        public string Coauteur_Collectivite;
        public string Coauteur_congres;
        public string Titre_de_forme;
        public string Titre;
        public string Titre_parallele;
        public string Mention_edition;
        public string Editeur;
        public string Annee_edition_bis;
        public string Pagination;
        public string Illustration;
        public string Format;
        public string Format_complementaire;
        public string Materiel;
        public string Materiel_complement;
        public string Collection;
        public string nb_de_collection;
        public string Sous_collection;
        public string nb_de_sous_collection;
        public string Notes_generales;
        public string Notes_de_contenu;
        public string Note_histoire_edition;
        public string Notes_sur_les_annexes;
        public string Genre_ou_theme;
        public string Public;
        public string Titre_original;
        public string Sujet_personne_physique;
        public string Sujet_collecivite;
        public string Sujet;
        public string Sujet_geographique;
        public string Titre_de_regroupement;
        public string num_du_titre_de_regroupement;
        public string grandes_bibs;
        public string Localisations_sections_adultes;
        public string Localisations_discotheques;
        public string Localisations_fonds_specialises_ou_reserves;
        public string Localisations_sections_jeunesse;
        public string Localisations_sections_poles_thematiques_ou_partitions;
        public string Localisations_videotheques;
        public string Support;
        public string Support_regroupe;
        public string Nombre_exemplaires_dans_le_support_majoritaire;
        public string Categorie_statistique;
        public string Classification_Dewey;
        public string Type_de_document_deduit_apres_la_categorie_statistique;
        public string Nb_exemplaires_pris_en_compte_pour_la_categorie_stat;
        public string Nombre_exemplaires;
        public string Prets_realises_en_6_mois;
        public string Pret_moyen_par_exemplaire_sur_6_mois;
        public string Localisations;
        public string Localisations_hors_reserve_centrale;
        public int Amelie;
        public int Andre_Malraux;
        public int Batignolles;
        public int Baudoyer;
        public int Beaugrenelle;
        public int Benjamin_Rabier;
        public int Brochant_Colette_Vivier;
        public int Buffon;
        public int Chaptal_;
        public int Chateau_eau;
        public int Clignancourt;
        public int Comptoir_Lame;
        public int Courcelles;
        public int Couronnes;
        public int Crimee;
        public int Diderot;
        public int Drouot;
        public int Edmond_Rostand;
        public int Europe;
        public int Faidherbe;
        public int Fessart;
        public int Flandre;
        public int Francois_Villon;
        public int Georges_Brassens;
        public int Glaciere;
        public int Goutte_Or;
        public int Gutenberg;
        public int Herge;
        public int Heure_Joyeuse;
        public int Isle_Saint_Louis;
        public int Italie;
        public int Jean_Pierre_Melville;
        public int La_Fontaine;
        public int Lancry;
        public int Louvre;
        public int Marguerite_Audoux;
        public int Marguerite_Yourcenar;
        public int Maurice_Genevoix;
        public int Mediatheque_Musicale_de_Paris;
        public int Mortier;
        public int Mouffetard;
        public int Musset;
        public int Parmentier;
        public int Personnel;
        public int Picpus_Helene_Berr;
        public int Place_des_Fetes;
        public int Plaisance_Aime_Cesaire;
        public int Porte_Montmartre;
        public int Port_Royal_Rainer_Maria_Rilke;
        public int Reserve_Centrale;
        public int Reunion_prefiguration;
        public int Saint_Blaise;
        public int Saint_Eloi;
        public int Saint_Fargeau;
        public int Saint_Simon;
        public int Sorbier;
        public int Trocadero_Germaine_Tillion;
        public int Valeyre;
        public int Vandamme;
        public int Vaugirard;
        public int Vivienne_Charlotte_Delbo;
       
        internal static Book Map(string[] bookArray)
        {
            Book book = new Book();

            int nbCols = 101;

            book.Langue = bookArray[0];
            book.AnneeEdition = bookArray[1];
            book.ISBN = bookArray[2];
            book.Indice = bookArray[3];
            book.Auteur_personne_physique = bookArray[4];
            book.Auteur_collectivite = bookArray[5];
            book.Coauteur = bookArray[6];
            book.Responsabilite = bookArray[7];
            book.Coauteur_Collectivite = bookArray[8];
            book.Coauteur_congres = bookArray[9];
            book.Titre_de_forme = bookArray[10];
            book.Titre = bookArray[11];
            book.Amelie = GetInt(bookArray[58]);
            book.Andre_Malraux = GetInt(bookArray[59]);
            book.Batignolles = GetInt(bookArray[60]);
            book.Baudoyer = GetInt(bookArray[61]);
            book.Beaugrenelle = GetInt(bookArray[62]);
            book.Benjamin_Rabier = GetInt(bookArray[63]);
            book.Brochant_Colette_Vivier = GetInt(bookArray[64]);
            book.Buffon = GetInt(bookArray[65]);
            book.Chaptal_ = GetInt(bookArray[66]);
            book.Chateau_eau = GetInt(bookArray[67]);
            book.Clignancourt = GetInt(bookArray[68]);
            book.Comptoir_Lame = GetInt(bookArray[69]);
            book.Courcelles = GetInt(bookArray[70]);
            book.Couronnes = GetInt(bookArray[71]);
            book.Crimee = GetInt(bookArray[72]);
            book.Diderot = GetInt(bookArray[73]);
            book.Drouot = GetInt(bookArray[74]);
            book.Edmond_Rostand = GetInt(bookArray[75]);
            book.Europe = GetInt(bookArray[76]);
            book.Faidherbe = GetInt(bookArray[77]);
            book.Fessart = GetInt(bookArray[78]);
            book.Flandre = GetInt(bookArray[79]);
            book.Francois_Villon = GetInt(bookArray[80]);
            book.Georges_Brassens = GetInt(bookArray[81]);
            book.Glaciere = GetInt(bookArray[82]);
            book.Goutte_Or = GetInt(bookArray[83]);
            book.Gutenberg = GetInt(bookArray[84]);
            book.Herge = GetInt(bookArray[85]);
            book.Heure_Joyeuse = GetInt(bookArray[86]);
            book.Isle_Saint_Louis = GetInt(bookArray[87]);
            book.Italie = GetInt(bookArray[88]);
            book.Jean_Pierre_Melville = GetInt(bookArray[89]);
            book.La_Fontaine = GetInt(bookArray[90]);
            book.Lancry = GetInt(bookArray[91]);
            book.Louvre = GetInt(bookArray[92]);
            book.Marguerite_Audoux = GetInt(bookArray[93]);
            book.Marguerite_Yourcenar = GetInt(bookArray[94]);
            book.Maurice_Genevoix = GetInt(bookArray[95]);
            book.Mediatheque_Musicale_de_Paris = GetInt(bookArray[96]);
            book.Mortier = GetInt(bookArray[97]);
            book.Mouffetard = GetInt(bookArray[98]);
            book.Musset = GetInt(bookArray[99]);
            book.Parmentier = GetInt(bookArray[100]);
            book.Personnel = GetInt(bookArray[101]);
            book.Picpus_Helene_Berr = GetInt(bookArray[102]);
            book.Place_des_Fetes = GetInt(bookArray[103]);
            book.Plaisance_Aime_Cesaire = GetInt(bookArray[104]);
            book.Porte_Montmartre = GetInt(bookArray[105]);
            book.Port_Royal_Rainer_Maria_Rilke = GetInt(bookArray[106]);
            book.Reserve_Centrale = GetInt(bookArray[107]);
            book.Reunion_prefiguration = GetInt(bookArray[108]);
            book.Saint_Blaise = GetInt(bookArray[109]);
            book.Saint_Eloi = GetInt(bookArray[110]);
            book.Saint_Fargeau = GetInt(bookArray[111]);
            book.Saint_Simon = GetInt(bookArray[112]);
            book.Sorbier = GetInt(bookArray[113]);
            book.Trocadero_Germaine_Tillion = GetInt(bookArray[114]);
            book.Valeyre = GetInt(bookArray[115]);
            book.Vandamme = GetInt(bookArray[116]);
            book.Vaugirard = GetInt(bookArray[117]);
            book.Vivienne_Charlotte_Delbo = GetInt(bookArray[118]);

            return book;
        }

        private static int GetInt(string p)
        {
            return string.IsNullOrWhiteSpace(p) ? 0 : Convert.ToInt32(p);
        }

        private static Dictionary<int, string> mapping = new Dictionary<int, string>()
        {
           {0, "Langue"},
           {1, "AnneeEdition"},
           {2, "ISBN"},
           {3, "Indice"},
           {4, "Auteur_personne_physique"},
           {5, "Auteur_collectivite"},
           {6, "Coauteur"},
           {7, "Responsabilite"},
           {8, "Coauteur_Collectivite"},
           {9, "Coauteur_congres"},
           {10, "Titre_de_forme"},
           {11, "Titre"},
           {12, "Titre_parallele"},
           {13, "Mention_edition"},
           {14, "Editeur"},
           {15, "Annee_edition_bis"},
           {16, "Pagination"},
           {17, "Illustration"},
           {18, "Format"},
           {19, "Format_complementaire"},
           {20, "Materiel"},
           {21, "Materiel_complement"},
           {22, "Collection"},
           {23, "nb_de_collection"},
           {24, "Sous_collection"},
           {25, "nb_de_sous_collection"},
           {26, "Notes_generales"},
           {27, "Notes_de_contenu"},
           {28, "Note_histoire_edition"},
           {29, "Notes_sur_les_annexes"},
           {30, "Genre_ou_theme"},
           {31, "Public"},
           {32, "Titre_original"},
           {33, "Sujet_personne_physique"},
           {34, "Sujet_collecivite"},
           {35, "Sujet"},
           {36, "Sujet_geographique"},
           {37, "Titre_de_regroupement"},
           {38, "num_du_titre_de_regroupement"},
           {39, "grandes_bibs"},
           {40, "Localisations_sections_adultes"},
           {41, "Localisations_discotheques"},
           {42, "Localisations_fonds_specialises_ou_reserves"},
           {43, "Localisations_sections_jeunesse"},
           {44, "Localisations_sections_poles_thematiques_ou_partitions"},
           {45, "Localisations_videotheques"},
           {46, "Support"},
           {47, "Support_regroupe"},
           {48, "Nombre_exemplaires_dans_le_support_majoritaire"},
           {49, "Categorie_statistique"},
           {50, "Classification_Dewey"},
           {51, "Type_de_document_deduit_apres_la_categorie_statistique"},
           {52, "Nb_exemplaires_pris_en_compte_pour_la_categorie_stat"},
           {53, "Nombre_exemplaires"},
           {54, "Prets_realises_en_6_mois"},
           {55, "Pret_moyen_par_exemplaire_sur_6_mois"},
           {56, "Localisations"},
           {57, "Localisations_hors_reserve_centrale"},
           {58, "Amelie"},
           {59, "Andre_Malraux"},
           {60, "Batignolles"},
           {61, "Baudoyer"},
           {62, "Beaugrenelle"},
           {63, "Benjamin_Rabier"},
           {64, "Brochant_Colette_Vivier"},
           {65, "Buffon"},
           {66, "Chaptal_"},
           {67, "Chateau_eau"},
           {68, "Clignancourt"},
           {69, "Comptoir_Lame"},
           {70, "Courcelles"},
           {71, "Couronnes"},
           {72, "Crimee"},
           {73, "Diderot"},
           {74, "Drouot"},
           {75, "Edmond_Rostand"},
           {76, "Europe"},
           {77, "Faidherbe"},
           {78, "Fessart"},
           {79, "Flandre"},
           {80, "Francois_Villon"},
           {81, "Georges_Brassens"},
           {82, "Glaciere"},
           {83, "Goutte_Or"},
           {84, "Gutenberg"},
           {85, "Herge"},
           {86, "Heure_Joyeuse"},
           {87, "Isle_Saint_Louis"},
           {88, "Italie"},
           {89, "Jean_Pierre_Melville"},
           {90, "La_Fontaine"},
           {91, "Lancry"},
           {92, "Louvre"},
           {93, "Marguerite_Audoux"},
           {94, "Marguerite_Yourcenar"},
           {95, "Maurice_Genevoix"},
           {96, "Mediathe   que_Musicale_de_Paris"},
           {97, "Mortier"},
           {98, "Mouffetard"},
           {99, "Musset"},
           {100, "Parmentier"},
           {101, "Personnel"},
           {102, "Picpus_Helene_Berr"},
           {103, "Place_des_Fetes"},
           {104, "Plaisance_Aime_Cesaire"},
           {105, "Porte_Montmartre"},
           {106, "Port_Royal_Rainer_Maria_Rilke"},
           {107, "Reserve_Centrale"},
           {108, "Reunion_prefiguration"},
           {109, "Saint_Blaise"},
           {110, "Saint_Eloi"},
           {111, "Saint_Fargeau"},
           {112, "Saint_Simon"},
           {113, "Sorbier"},
           {114, "Trocadero_Germaine_Tillion"},
           {115, "Valeyre"},
           {116, "Vandamme"},
           {117, "Vaugirard"},
           {118, "Vivienne_Charlotte_Delbo"}
        };
    }
}
