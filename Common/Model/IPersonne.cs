﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public interface IPersonne
    {
        int Id { get; set; }
        string Nom { get; set; }
        string Prenom { get; set; }
        int Age { get; set;  }
    }
}
