﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Services
{
    public class CalculatorService
    {
        /// <summary>
        /// Computes for n seconds
        /// </summary>
        /// <param name="nbSeconds">The number of seconds.</param>
        /// <returns></returns>
        public static int Compute(double nbSeconds)
        {
            var start = DateTime.Now;

            Debug.WriteLine(string.Format("[{0}] Computing on Background={1}, on ThreadPoolThread={2} ",
                                                                Thread.CurrentThread.ManagedThreadId,
                                                                Thread.CurrentThread.IsThreadPoolThread,
                                                                Thread.CurrentThread.IsBackground));

            double result;
            do
            {
                result = Math.Exp(10);

            } while ((DateTime.Now - start).TotalSeconds < nbSeconds);

            return (int)result;
        }

        public int nonStaticCompute()
        {
            return 2;
        }
    }
}
