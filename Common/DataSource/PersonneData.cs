﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Common.DataSource
{
    public class PersonneData
    {
        IList<IPersonne> _data;

        public PersonneData()
        {
            _data = new List<IPersonne>()
                {
                {new Personne(1, "CAILLARD", "Anthyme", 29)},
                {new Personne(2, "HAUBERT", "Geoffroy", 30)},
                {new Personne(3, "EVEN", "Peter", 35)},
                {new Personne(4, "FAUQUEMBERGUE", "Eric", 37)},
                {new Personne(5, "SIBER", "Eric", 31)},
                {new Personne(6, "WONG", "Thomas", 19)},
                {new Personne(7, "NICOLAS", "Loic", 27)},
                {new Personne(8, "FROUCHT", "Elie", 29)},
                {new Personne(9, "COULIBALY", "Adam", 33)},
                {new Personne(10, "TOURNIER", "Thierry", 17)},
                {new Personne(11, "BEN ASKER", "Anis", 27)}
                };
        }

        public IList<IPersonne> GetAll()
        {
            return _data;
        }

        public IEnumerable<IPersonne> GetAllOneByOne()
        {
            IEnumerable<IPersonne> personnes = new List<IPersonne>(11);
            foreach (IPersonne p in _data)
            {
                Thread.Sleep(500);
                yield return p;
            }
        }
    }
}
