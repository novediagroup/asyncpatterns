﻿using Common;
using Common.DataSource;
using Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tpl.ConcurrentCollections.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            PersonneData personneData = new PersonneData();
            IList<IPersonne> personnes = personneData.GetAll();

            #region Display
            Console.WriteLine("[{0}] nb processors : {1}", Thread.CurrentThread.ManagedThreadId, System.Environment.ProcessorCount);
            Thread.Sleep(1000);
            Console.WriteLine("[{0}] Starting...", Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(300);
            #endregion

            DateTime startTime = DateTime.Now;

            Stack<IPersonne> stackOfPersonne = new Stack<IPersonne>();
            List<Task> tasks = new List<Task>();

            // >>> 1. Simple Foreach
            foreach (IPersonne p in personnes)
            {
                Task task = Task.Factory.StartNew(() =>
                {
                    stackOfPersonne.Push(p);
                    //Console.WriteLine("[{0}] > Pushed n°{1} {2} {3} ...", Thread.CurrentThread.ManagedThreadId, p.Id, p.Prenom, p.Nom);
                });

                tasks.Add(task);
            }

            Task.WaitAll(tasks.ToArray());

            foreach (IPersonne p in stackOfPersonne)
            {
                Console.WriteLine("[{0}] > Found n°{1} {2} {3} ...", Thread.CurrentThread.ManagedThreadId, p.Id, p.Prenom, p.Nom);
            }

            DateTime endTime = DateTime.Now;
            TimeSpan elapsedTime = endTime.Subtract(startTime);
            Console.WriteLine("[{0}] Simple Foreach completed in {1}", System.Threading.Thread.CurrentThread.ManagedThreadId, elapsedTime);

            Console.ReadKey();
        }
    }
}
