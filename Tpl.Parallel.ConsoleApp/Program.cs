﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Threading;
using Common;
using Common.DataSource;
using Common.Services;

namespace Tpl.Parallel.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            PersonneData personneData = new PersonneData();
            IList<IPersonne> personnes = personneData.GetAll();

            #region Display
            Console.WriteLine("[{0}] nb processors : {1}", Thread.CurrentThread.ManagedThreadId, System.Environment.ProcessorCount);
            Thread.Sleep(1000);
            Console.WriteLine("[{0}] Starting...", Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(300);
            #endregion

            DateTime startTime = DateTime.Now;

            // >>> 1. Simple Foreach
            //foreach (IPersonne p in personnes)
            //{
            //    p.Age = CalculatorService.Compute(2);
            //    Console.WriteLine("[{0}] > Processed n°{1} {2} {3} ...", Thread.CurrentThread.ManagedThreadId, p.Id, p.Prenom, p.Nom);
            //}

            //DateTime endTime = DateTime.Now;
            //TimeSpan elapsedTime = endTime.Subtract(startTime);
            //Console.WriteLine("[{0}] Simple Foreach completed in {1}", System.Threading.Thread.CurrentThread.ManagedThreadId, elapsedTime);

            // >>> 2. Parallel Foreach
            ParallelLoopResult result = System.Threading.Tasks.Parallel.ForEach<IPersonne>(personnes, (p) =>
            {
                p.Age = CalculatorService.Compute(2);
                Console.WriteLine("[{0}] > Processed n°{1} {2} {3} ...", Thread.CurrentThread.ManagedThreadId, p.Id, p.Prenom, p.Nom);
            });

            DateTime endTime = DateTime.Now;
            TimeSpan elapsedTime = endTime.Subtract(startTime);
            Console.WriteLine("[{0}] ParallelLoop Foreach completed in {2} ({1})", System.Threading.Thread.CurrentThread.ManagedThreadId, result.IsCompleted, elapsedTime);

            Console.ReadKey();
        }
    }
}
