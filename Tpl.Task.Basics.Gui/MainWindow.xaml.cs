﻿using Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace Tpl.Tasks.Basics.Gui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const double _time = 2;

        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        ///  Sequential exécution
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Sync_OnClick(object sender, RoutedEventArgs e)
        {
            StartProgress();
            ClearScr();

            Result.Text = CalculatorService.Compute(_time).ToString();

            EndProgress();
        }

        /// <summary>
        /// Basic Task execution by default TaskScheduler
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Task_OnClick(object sender, RoutedEventArgs e)
        {
            StartProgress();
            ClearScr();

            // Définition de la tâche : Status = Started
            Task<int> task = Task.Factory.StartNew<int>(() => { return CalculatorService.Compute(4); });

            Task complete = task.ContinueWith((antecedent) =>
            {
                Result.Text = antecedent.Result.ToString();
                EndProgress();
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        /// <summary>
        /// Handles the OnClick event of the Task2 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Task2_OnClick(object sender, RoutedEventArgs e)
        {
            StartProgress();
            ClearScr();

            // Définition de la tâche : Status = Started
            Task<int> task = Task.Factory.StartNew<int>(() => { return CalculatorService.Compute(4); });
            
            Task complete = task.ContinueWith((antecedent) => 
                {
                    Result.Text = antecedent.Result.ToString();
                    EndProgress();
                });
        }

        /// <summary>
        /// WaiAll // ContinueWhenAll
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Task3_OnClick(object sender, RoutedEventArgs e)
        {
            StartProgress();
            ClearScr();

            // Définition de la tâche : Status = Started
            Task<int> task1 = Task.Factory.StartNew<int>(() => { return CalculatorService.Compute(4); });
            Task<int> task2 = Task.Factory.StartNew<int>(() => { return CalculatorService.Compute(4); });
            Task<int> task3 = Task.Factory.StartNew<int>(() => { return CalculatorService.Compute(4); });

            // Synchronisation
            Task.WaitAll(new Task<int>[]{task1, task2, task3});

            Task.Factory.ContinueWhenAll(new Task<int>[] { task1, task2, task3 }, 
                (tasks) => { Result.Text = tasks.Sum(x => x.Result).ToString(); });  

            // Continuation
            Task complete = task1.ContinueWith((antecedent) =>
            {
                Result.Text = antecedent.Result.ToString();
                EndProgress();
            }, TaskContinuationOptions.LongRunning);
        }


        private void Task4_OnClick(object sender, RoutedEventArgs e)
        {
            StartProgress();
            ClearScr();

            // Définition de la tâche : Status = Started
            Task<int> task = Task.Factory.StartNew<int>(() => { return CalculatorService.Compute(4); });

            // Continuation
            Task complete = task.ContinueWith((antecedent) =>
            {
                Result.Text = antecedent.Result.ToString();
                EndProgress();
            });
        }

        private void ClearScr()
        {
            Result.Text = "";
        }

        private void EndProgress()
        {
            Progress.IsIndeterminate = false;
        }

        private void StartProgress()
        {
            Progress.IsIndeterminate = true;
        }
    }
}
