﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tpl.ExceptionHandling
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ReadKey();

            //Console.WriteLine("SilentException");
            //SilentException();
            //Console.ReadKey();

            //Console.WriteLine("HandleWithWait");
            //HandleWithWait();
            //Console.ReadKey();

            //Console.WriteLine("HandleWithResult");
            //HandleWithResult();
            //Console.ReadKey();

            Console.WriteLine("HandleInContinuation");
            HandleInContinuation();
            Console.ReadKey();
        }

        private static void SilentException()
        {
            Task.Run(() =>
            {
                throw new Exception("Silent Exception");
            });
        }

        private static void HandleWithWait()
        {
            var task = Task.Run(() =>
            {
                throw new Exception("Task Exception");
            });

            try
            {
                task.Wait();
            }
            catch (AggregateException ex)
            {
                Console.WriteLine(ex.InnerException.Message);
            }
        }

        private static void HandleWithResult()
        {
            var task = Task.Run(() =>
            {
                throw new Exception("Task Exception");
                return 0;
            });

            try
            {
                var result = task.Result;
            }
            catch (AggregateException ex)
            {
                Console.WriteLine(ex.InnerException.Message);
            }
        }

        private static void HandleInContinuation()
        {
            var task = Task.Run(() =>
            {
                throw new Exception("Task Exception");
            }).ContinueWith(t =>
            {
                if (t.Exception != null)
                {
                    Console.WriteLine(t.Exception.InnerException.Message);
                }
            });

            //task.Wait();
        }
    }
}
