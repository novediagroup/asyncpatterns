﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncWait.ConsoleApp
{
    class DisposableOutput : IDisposable
    {
        public DisposableOutput()
        {
            Console.WriteLine("Using start");
        }

        public void Dispose()
        {
            Console.WriteLine("Using end");
        }
    }

    class UsingOutput
    {
        public static Task Task1Async()
        {
            using (new DisposableOutput())
            {
                return Work.WorkAsync();
            }
        }

        public static Task Task2Async()
        {
            var output = new DisposableOutput();
            return Work.WorkAsync().ContinueWith(t =>
            {
                var result = t.Result;
                output.Dispose();
            });
        }

        public static async Task AwaitAsync()
        {
            using (new DisposableOutput())
            {
                var result = await Work.WorkAsync();
            }
        }
    }
}
