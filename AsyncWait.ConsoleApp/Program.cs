﻿using System;
using System.Threading.Tasks;

namespace AsyncWait.ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.ReadKey();

            //BasicOutputAsync().Wait();
            //ChainAsync().Wait();
            //ParallelAsync().Wait();
            //UsingOutputAsync().Wait();
            //UsingExceptionAsync().Wait();
            MissingExceptionAsync().Wait();
            CustomAwaiterAsync().Wait();
            SequentialQueueAsync().Wait();

            Console.ReadKey();
        }

        private static async Task UsingExceptionAsync()
        {
            try
            {
                Console.WriteLine("- Using Exception task 1");
                await UsingExceptionOutput.Task1Async();
            }
            catch (Exception) { }
            Console.ReadKey();

            try
            {
                Console.WriteLine("- Using Exception task 2");
                await UsingExceptionOutput.Task2Async();
            }
            catch (Exception) { }
            Console.ReadKey();

            try
            {
                Console.WriteLine("- Using Exception task 3");
                await UsingExceptionOutput.Task3Async();
            }
            catch (Exception) { }
            Console.ReadKey();

            try
            {
                Console.WriteLine("- Using Exception task 4");
                await UsingExceptionOutput.Task4Async();
            }
            catch (Exception) { }
            Console.ReadKey(); { }

            try
            {
                Console.WriteLine("- Using Exception async");
                await UsingExceptionOutput.AwaitAsync();
            }
            catch (Exception) { }
            Console.ReadKey();

            try
            {
                Console.WriteLine("- Using Exception async 2");
                await UsingExceptionOutput.Await2Async();
            }
            catch (Exception) { }
            Console.ReadKey();
        }

        private static async Task UsingOutputAsync()
        {
            Console.WriteLine("- Using task 1");
            await UsingOutput.Task1Async();
            Console.ReadKey();

            Console.WriteLine("- Using task 2");
            await UsingOutput.Task2Async();
            Console.ReadKey();

            Console.WriteLine("- Using async");
            await UsingOutput.AwaitAsync();
            Console.ReadKey();
        }

        private static async Task BasicOutputAsync()
        {
            Console.WriteLine("- Basic task 1");
            await BasicOutput.Task1Async();
            Console.ReadKey();

            Console.WriteLine("- Basic task 2");
            await BasicOutput.Task2Async();
            Console.ReadKey();

            Console.WriteLine("- Basic async");
            await BasicOutput.AwaitAsync();
            Console.ReadKey();
        }

        private static async Task ChainAsync()
        {
            Console.WriteLine("- Chain task 1");
            await Chain.Task1Async();
            Console.ReadKey();

            Console.WriteLine("- Chain task 2");
            await Chain.Task2Async();
            Console.ReadKey();

            Console.WriteLine("- Chain async");
            await Chain.AwaitAsync();
            Console.ReadKey();
        }

        private static async Task ParallelAsync()
        {
            Console.WriteLine("- Parallel task 1");
            await Parallel.Task1Async();
            Console.ReadKey();

            Console.WriteLine("- Parallel async");
            await Parallel.AwaitAsync();
            Console.ReadKey();
        }

        private static async Task MissingExceptionAsync()
        {
            Console.WriteLine("- Missing Exception First");
            await MissingExceptions.FirstAsync();
            Console.ReadKey();

            Console.WriteLine("- Missing Exception Second");
            await MissingExceptions.SecondAsync();
            Console.ReadKey();

            Console.WriteLine("- Missing Exception All");
            await MissingExceptions.AllAsync();
            Console.ReadKey();

            Console.WriteLine("- Missing Exception All continue with");
            await MissingExceptions.AllContinueWithAsync();
            Console.ReadKey();
        }

        private static async Task CustomAwaiterAsync()
        {
            Console.WriteLine("- Await Int");
            await CustomAwaiter.AwaitIntAsync();
            Console.WriteLine("End Await Int");

            Console.WriteLine("- Await DateTime");
            await CustomAwaiter.AwaitDateTimeAsync();
            Console.WriteLine("End Await DateTime");

            Console.WriteLine("- Await TimeSpan");
            await CustomAwaiter.AwaitTimeSpanAsync();
            Console.WriteLine("End Await TimeSpan");

            Console.WriteLine("- Await My");
            await CustomAwaiter.AwaitMyAsync();
            Console.WriteLine("End Await My");
        }

        private static async Task SequentialQueueAsync()
        {
            Console.WriteLine("- Sequential queue");
            SequentialQueue.DoMultipleActionsAsync();
            Console.WriteLine("End sequential queue call");

            Console.WriteLine("Dispose sequential queue");
            SequentialQueue.Dispose();

            Console.ReadKey();
        }
    }
}