﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncWait.ConsoleApp
{
    class Parallel
    {
        private static Task<string> Call1Async()
        {
            return Task.Run(() => "Call 1");
        }

        private static Task<string> Call2Async()
        {
            return Task.Run(() => "Call 2");
        }

        private static Task<string> Call3Async()
        {
            return Task.Run(() => "Call 3");
        }

        public static Task Task1Async()
        {
            var task = Task.Run(() =>
            {
                var call1 = Call1Async();
                var call2 = Call2Async();
                var call3 = Call3Async();
                call1.Wait();
                call2.Wait();
                call3.Wait();
                Console.WriteLine(call1.Result);
                Console.WriteLine(call2.Result);
                Console.WriteLine(call3.Result);
            });
            return task;
        }

        public static async Task AwaitAsync()
        {
            var call1 = Call1Async();
            var call2 = Call2Async();
            var call3 = Call3Async();

            Console.WriteLine(await call1);
            Console.WriteLine(await call2);
            Console.WriteLine(await call3);
        }
    }
}
