﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncWait.ConsoleApp
{
    class CustomAwaiter
    {
        public static async Task AwaitIntAsync()
        {
            await 1000;
        }

        public static async Task AwaitDateTimeAsync()
        {
            await new TimeSpan(0, 0, 1);
        }

        public static async Task AwaitTimeSpanAsync()
        {
            await DateTime.Now.AddSeconds(1);
        }

        public static async Task AwaitMyAsync()
        {
            var myAwaitable = new MyAwaitable();

            Task.Run(async () =>
            {
                await Task.Delay(1000);
                myAwaitable.Done("externaly controled end");
            });

            var res1 = await myAwaitable;
            Console.WriteLine("My finished 1: " + res1);
            var res2 = await myAwaitable;
            Console.WriteLine("My finished 2: " + res2);
        }
    }

    static class IntegerAwaiter
    {
        public static TaskAwaiter GetAwaiter(this int value)
        {
            return Task.Delay(value).GetAwaiter();
        }
    }

    static class TimeSpanAwaiter
    {
        public static TaskAwaiter GetAwaiter(this TimeSpan value)
        {
            return Task.Delay(value).GetAwaiter();
        }
    }

    static class DateTimeAwaiter
    {
        public static TaskAwaiter GetAwaiter(this DateTime value)
        {
            return Task.Delay(value - DateTime.Now).GetAwaiter();
        }
    }

    public class MyAwaitable
    {
        private readonly ICollection<MyAwaiter> _awaiters = new Collection<MyAwaiter>();

        public bool IsCompleted { get; private set; }
        public string Result { get; set; }

        public MyAwaiter GetAwaiter()
        {
            var awaiter = new MyAwaiter(this);
            _awaiters.Add(awaiter);
            return awaiter;
        }

        public void Done(string result)
        {
            Result = result;
            IsCompleted = true;
            foreach (var awaiter in _awaiters)
            {
                awaiter.Done();
            }
        }
    }

    public class MyAwaiter : INotifyCompletion
    {
        private readonly MyAwaitable _awaitable;
        public bool IsCompleted { get { return _awaitable.IsCompleted; } }
        readonly EventWaitHandle _waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset);

        public MyAwaiter(MyAwaitable awaitable)
        {
            _awaitable = awaitable;
        }

        public void OnCompleted(Action continuation)
        {
            continuation();
        }

        public string GetResult()
        {
            if (!IsCompleted) _waitHandle.WaitOne();
            return _awaitable.Result;
        }

        public void Done()
        {
            _waitHandle.Set();
        }
    }
}
