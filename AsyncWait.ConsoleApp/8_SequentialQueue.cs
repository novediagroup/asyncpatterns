﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncWait.ConsoleApp
{
    class SequentialQueue
    {
        public static void DoMultipleActionsAsync()
        {
            RunSequentialy(Work);
            RunSequentialy(Work);
            RunSequentialy(Work);
            RunSequentialy(Work);
            RunSequentialy(Work);
        }

        public static void Dispose()
        {
            works.CompleteAdding();
        }

        static BlockingCollection<Func<Task>> works = new BlockingCollection<Func<Task>>();
        private static int currentActor;
        private static bool isRunning;

        private static async void RunSequentialy(Func<Task> work)
        {
            works.Add(work);

            if (isRunning)return;

            Console.WriteLine("Start sequential loop");
            isRunning = true;
            while (!works.IsCompleted)
            {
                var action = works.Take();
                await action();
            }
            isRunning = false;
            Console.WriteLine("End sequential loop");
        }

        private static async Task Work()
        {
            int id = currentActor++;
            Console.WriteLine("Start actor " + id);
            await Task.Delay(1000);
            Console.WriteLine("End actor " + id);
        }
    }
}
