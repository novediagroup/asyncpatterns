﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncWait.ConsoleApp
{
    class Chain
    {
        private static Task<string> Call1Async()
        {
            return Task.Run(() => "Call 1");
        }

        private static Task<string> Call2Async()
        {
            return Task.Run(() => "Call 2");
        }

        private static Task<string> Call3Async()
        {
            return Task.Run(() => "Call 3");
        }

        public static Task Task1Async()
        {
            var task = Call1Async()
                .ContinueWith(t =>
                {
                    Console.WriteLine(t.Result);
                    return Call2Async();
                })
                .ContinueWith(t =>
                {
                    var oldTask = t.Unwrap();
                    Console.WriteLine(oldTask.Result);
                    return Call3Async();
                }).ContinueWith(t =>
                {
                    var oldTask = t.Unwrap();
                    Console.WriteLine(oldTask.Result);
                });
            return task;
        }

        public static Task Task2Async()
        {
            var task = Call1Async()
                .ContinueWith(t =>
                {
                    Console.WriteLine(t.Result);
                    var call2 = Call2Async();
                    call2.Wait();
                    Console.WriteLine(call2.Result);
                    var call3 = Call3Async();
                    call3.Wait();
                    Console.WriteLine(call3.Result);
                });
            return task;
        }

        public static async Task AwaitAsync()
        {
            Console.WriteLine(await Call1Async());
            Console.WriteLine(await Call2Async());
            Console.WriteLine(await Call3Async());
        }
    }
}
