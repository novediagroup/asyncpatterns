﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncWait.ConsoleApp
{
    class BasicOutput
    {
        public static Task Task1Async()
        {
            Console.WriteLine("Task start");
            var task = Work.WorkAsync();
            Console.WriteLine("Task end");
            return task;
        }

        public static Task Task2Async()
        {
            Console.WriteLine("Task start");
            return Work.WorkAsync().ContinueWith(t =>
            {
                Console.WriteLine("Task end:" + t.Result);
            });
        }

        public static async Task AwaitAsync()
        {
            Console.WriteLine("Out task start");
            var result = await Work.WorkAsync();
            Console.WriteLine("Out task end:" + result);
        }
    }
}
