﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncWait.ConsoleApp
{
    class UsingExceptionOutput
    {
        public static Task Task1Async()
        {
            using (new DisposableOutput())
            {
                return Work.WorkExceptionAsync();
            }
        }

        public static Task Task2Async()
        {
            var output = new DisposableOutput();
            return Work.WorkExceptionAsync().ContinueWith(t =>
            {
                var result = t.Result;
                output.Dispose();
            });
        }

        public static Task Task3Async()
        {
            var output = new DisposableOutput();
            return Work.WorkExceptionAsync().ContinueWith(t =>
            {
                var result = t.Result;
            }).ContinueWith(t =>
            {
                output.Dispose();
            });
        }

        public static Task Task4Async()
        {
            var output = new DisposableOutput();
            return Work.WorkExceptionAsync().ContinueWith(t =>
            {
                if (t.Exception == null)
                {
                    var result = t.Result;
                }
                output.Dispose();
            });
        }

        public static async Task AwaitAsync()
        {
            using (new DisposableOutput())
            {
                var result = await Work.WorkExceptionAsync();
            }
        }

        public static async Task Await2Async()
        {
            using (new DisposableOutput())
            {
                try
                {
                    var result = await Work.WorkExceptionAsync();
                }
                catch (Exception ex)
                {
                    
                }
            }
        }
    }
}
