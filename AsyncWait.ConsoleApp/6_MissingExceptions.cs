﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncWait.ConsoleApp
{
    class MissingExceptions
    {
        private static async Task<int> CrashAfterAsync(int seconds)
        {
            await Task.Delay(seconds * 1000);
            throw new Exception("Crashed after " + seconds + "s");
        }

        public static async Task<int> FirstAsync()
        {
            try
            {
                var crash1 = CrashAfterAsync(1);
                var crash2 = CrashAfterAsync(2);
                return await crash1 + await crash2;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static async Task<int> SecondAsync()
        {
            try
            {
                var crash1 = CrashAfterAsync(2);
                var crash2 = CrashAfterAsync(1);
                return await crash1 + await crash2;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static async Task<int> AllAsync()
        {
            try
            {
                var crash1 = CrashAfterAsync(2);
                var crash2 = CrashAfterAsync(1);
                var all = await Task.WhenAll(crash1, crash2);
                return all.Sum();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static async Task<int> AllContinueWithAsync()
        {
            try
            {
                var crash1 = CrashAfterAsync(2);
                var crash2 = CrashAfterAsync(1);
                var all = Task.WhenAll(crash1, crash2);
                await all.ContinueWith(t => { });
                return all.Result.Sum();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
