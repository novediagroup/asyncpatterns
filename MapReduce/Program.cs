﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapReduce.ConsoleApp
{
    class Program
    {

        static void Main(string[] args)
        {
            if (args == null || args.Length == 0)
            {
                Console.WriteLine("Entrez un mot!");
                args = new string[1];
                args[0] = Console.ReadLine();
            }

            MapReduce(args);

            Console.ReadLine();
        }

        static Dictionary<string, int> reduceList = new Dictionary<string, int>();

        static void MapReduce(string[] args)
        {
            string path = @"C:\temp\bibliotheques_localisations_exemplaires_jan_2009.csv";

            DateTime dtBegin = DateTime.Now;

            string wordToSearch = args[0].ToLower();
            //Console.WriteLine("Nombre de coeurs: {0}", System.Environment.ProcessorCount);

            using (var reader = new FileReader(path))
            {
                Parallel.ForEach(reader.Lines,
                    () =>
                    {
                        return new Dictionary<string, int>();
                    },

                    (line, _, localList) =>
                    {
                        string[] words = line.Split(new char[] { '\t', ' ', '.', ',', '/', '\\', '\'', '-', ';', '[', ']' });

                        //Map
                        foreach (string word in words)
                        {
                            if (word.ToLower() == wordToSearch)
                            {
                                if (localList.ContainsKey(word.ToLower()))
                                {
                                    localList[word.ToLower()] += 1;
                                }
                                else
                                {
                                    localList.Add(word.ToLower(), 1);
                                }
                            }
                        }

                        return localList;
                    },

                    (localList) =>
                    {
                        if (localList.Count > 0)
                        {
                            foreach (KeyValuePair<string, int> kvp in localList)
                            {
                                //Reduce
                                if (reduceList.ContainsKey(kvp.Key.ToLower()))
                                {
                                    reduceList[kvp.Key] += kvp.Value;
                                }
                                else
                                {
                                    reduceList.Add(kvp.Key, kvp.Value);
                                }
                            }
                        }
                    });
            }

            KeyValuePair<string, int> kvp2 = reduceList.First<KeyValuePair<string, int>>(k => k.Key.ToLower() == wordToSearch);

            if (string.IsNullOrEmpty(kvp2.Key))
            {
                Console.WriteLine("Recherche : " + wordToSearch + " Non trouvé!");
            }
            else
            {
                Console.WriteLine("Recherche : " + wordToSearch + " nombre d'occurence : " + kvp2.Value.ToString());
            }

            Console.WriteLine("temps écoulé: {0}", (DateTime.Now - dtBegin).ToString());
        }
    }
}
