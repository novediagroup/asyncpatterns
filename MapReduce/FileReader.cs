﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MapReduce.ConsoleApp
{
    public class FileReader : IDisposable
    {
        private readonly StreamReader _reader;

        public FileReader(string path)
        {
            _reader = new StreamReader(path);
        }

        public IEnumerable<string> Lines
        {
            get
            {
                while (_reader.EndOfStream == false)
                {
                    yield return _reader.ReadLine();
                }
            }
        }

        public void Dispose()
        {
            _reader.Close();
        }
    }
}
