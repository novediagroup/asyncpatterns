﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AsyncAwait.Gui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Sync_OnClick(object sender, RoutedEventArgs e)
        {
            StartProgress();
            Result.Text = "";
            Result.Text = Compute().ToString();
            EndProgress();
        }

        private void Task_OnClick(object sender, RoutedEventArgs e)
        {
            StartProgress();
            Result.Text = "";
            ComputeAsync().ContinueWith(t =>
            {
                Result.Text = Compute().ToString();
                EndProgress();
            });
        }

        private void Task2_OnClick(object sender, RoutedEventArgs e)
        {
            StartProgress();
            Result.Text = "";
            ComputeAsync().ContinueWith(t =>
            {
                Result.Text = t.Result.ToString();
                EndProgress();
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private async void Async_OnClick(object sender, RoutedEventArgs e)
        {
            StartProgress();
            Result.Text = "";
            Result.Text = (await ComputeAsync()).ToString();
            EndProgress();
        }

        CancellationTokenSource cts;

        private async void Cancelable_OnClick(object sender, RoutedEventArgs e)
        {
            StartProgress();
            Result.Text = "";
            cts = new CancellationTokenSource();
            var token = cts.Token;

            Result.Text = await Task.Run(async () =>
            {
                var res1 = await ComputeAsync();
                token.ThrowIfCancellationRequested();
                var res2 = await ComputeAsync();
                token.ThrowIfCancellationRequested();
                var res3 = await ComputeAsync();
                token.ThrowIfCancellationRequested();

                return res1 + res2 + res3;
            }, token)
            .ContinueWith(t => t.IsCanceled ? "Canceled" : t.Result.ToString());

            EndProgress();
        }

        private async void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            cts.Cancel();
        }

        private int Compute()
        {
            var start = DateTime.Now;

            double result;
            do
            {
                result = Math.Exp(10);

            } while ((DateTime.Now - start).TotalSeconds < 2);

            return (int)result;
        }

        private Task<int> ComputeAsync()
        {
            return Task.Run(() => Compute());
        }

        private void EndProgress()
        {
            Progress.IsIndeterminate = false;
        }

        private void StartProgress()
        {
            Progress.IsIndeterminate = true;
        }
    }
}
